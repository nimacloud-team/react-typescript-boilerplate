import request from './http';

class SampleService {
  getAll() {
    return request.get('/users');
  }
}

export default new SampleService();
