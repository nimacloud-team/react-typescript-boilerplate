import './button.css';

import { mdiXml } from '@mdi/js';
import Icon from '@mdi/react';
import React from 'react';

interface Props {
  variant?: string;
  icon?: string;
  label?: string;
  onClick: () => void;
}

const Button: React.FC<Props> = ({ label, onClick }) => {
  let buttonClass = 'container';
  return (
    <button onClick={onClick} className={buttonClass}>
      <Icon size={1} path={mdiXml}></Icon>
      {label}
    </button>
  );
};

export default Button;
