import './styles/app.css';

import React from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';

import NavBar from './components/navBar/navBar';
import Dashboard from './pages/dashboard';
import Login from './pages/login';
import Projects from './pages/projects';

function App() {
  const isLogged = () => {
    return localStorage.getItem('token');
  };
  return (
    <div className="App">
      <header className="App-header">
        <NavBar></NavBar>
      </header>
      <section>
        <Router basename="/#/">
          <Switch>
            <Route
              path="/login"
              render={() => {
                return !isLogged() ? <Login /> : <Redirect to="/" />;
              }}
            />
            <Route path="/" exact>
              <Redirect to="/dashboard" />
            </Route>
            <Route
              path="/dashboard"
              render={() => {
                return isLogged() ? <Dashboard /> : <Redirect to="/login" />;
              }}
            />
            <Route
              path="/projects"
              render={() => {
                return isLogged() ? <Projects /> : <Redirect to="/login" />;
              }}
            />
          </Switch>
        </Router>
      </section>
    </div>
  );
}

export default App;
