import { useHistory } from 'react-router-dom';

import SampleService from '../services/sample.service';

function Dashboard() {
  const history = useHistory();

  const getUsers = async () => {
    SampleService.getAll()
      .then((response) => {
        console.log(response);
      })
      .catch((e) => console.log(e));
  };

  const logout = () => {
    localStorage.removeItem('token');
    history.push('/');
  };

  return (
    <div>
      <button onClick={() => getUsers()}>Get Users</button>
      <button onClick={logout}>Logout</button>
    </div>
  );
}

export default Dashboard;
