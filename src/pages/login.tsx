import { Redirect } from 'react-router';

function Login() {
  const url = new URL(window.location.href.replace('/#/', '/'));
  const newToken = url.searchParams.get('token') as string;
  if (newToken) {
    localStorage.setItem('token', newToken);
    return <Redirect to="/" />;
  } else {
    const redirectURI = (process.env.REACT_APP_LOGIN_URL +
      encodeURIComponent(window.location.origin)) as string;
    window.location.href = redirectURI;
    return null;
  }
}

export default Login;
