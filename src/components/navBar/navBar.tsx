import './navBar.css';

import { useTranslation } from 'react-i18next';

import nervLogo from '../../assets/svg/nervLogo.svg';

const NavBar = () => {
  const { t } = useTranslation();

  return (
    <div className="navBar container">
      <div>
        <img className="logo" src={nervLogo} alt={t('label_nervLogo')}></img>
      </div>
      <div className="navItemContainer">
        <a href="/dashboard" className="navItem">
          {t('label_navbar_dashboard')}
        </a>
        <a href="/projects" className="navItem">
          {t('label_navbar_projects')}
        </a>
      </div>
    </div>
  );
};

export default NavBar;
