import axios from 'axios';

console.log(process.env.BASE_URL);

const axiosApiInstance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
});

axiosApiInstance.interceptors.request.use(
  async (config) => {
    const token = localStorage.getItem('token') || 'le-token';
    config.headers = {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

export default axiosApiInstance;
